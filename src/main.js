import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "./assets/styles/main.css";
import VueMask from "@devindex/vue-mask"
createApp(App).use(store).use(VueMask).use(router).mount("#app");
